1-Write a mongodb query to find all employee with similar email domains 
(Hint: with "bugsall.com" domain, there are three employees)
db.users.find({"email":/.*@bugsall.com.*/}).count()
1527
db.users.find({"email":/.*@bugsall.com.*/}).pretty()
{
  "_id" : ObjectId("5f631528819dc2c0f40c6446"),
  "employeeId" : "863c40e2-96e6-4d61-91e6-2733a0276464",
  "picture" : "http://placehold.it/32x32",
  "age" : 27,
  "eyeColor" : "blue",
  "name" : "Latonya Burch",
  "gender" : "female",
  "email" : "latonyaburch@bugsall.com",
  "phone" : "+1 (834) 574-2647",
  "salary" : 133590,
  "address" : "822 Elton Street, Teasdale, Iowa, 7678",
  "joinDate" : "2016-02-29T07:15:17 -06:-30",
  "travelledCountries" : [ ],
  "greeting" : "Hello, Latonya Burch! You have 3 unread messages."
}
{
  "_id" : ObjectId("5f631528819dc2c0f40c6447"),
  "employeeId" : "822e0c99-5d96-40e3-ad13-e4fc61a07d10",
  "picture" : "http://placehold.it/32x32",
  "age" : 29,
  "eyeColor" : "green",
  "name" : "Silvia Page",
  "gender" : "female",
  "email" : "silviapage@bugsall.com",
  "phone" : "+1 (834) 468-2901",
  "salary" : 196518,
  "address" : "947 Manor Court, Brambleton, Arizona, 1217",
  "joinDate" : "2017-08-04T09:49:38 -06:-30",
  "travelledCountries" : [
          "Tonga",
          "Greenland",
          "Barbados"
  ],
  "greeting" : "Hello, Silvia Page! You have 6 unread messages."
}
Type "it" for more


2-Write mongodb query to find all employees who travelled to atleast one country.
db.users.find({travelledCountries:{$exists:true},$where:"this.travelledCountries.length>=1"}).count()
1244

db.users.find({travelledCountries:{$exists:true},$where:"this.travelledCountries.length>=1"}).pretty()
{
        "_id" : ObjectId("5f631528819dc2c0f40c643b"),
        "employeeId" : "7e656b03-381d-41fe-b24c-f7714803f35d",
        "picture" : "http://placehold.it/32x32",
        "age" : 31,
        "eyeColor" : "blue",
        "name" : "Mcneil Hoover",
        "gender" : "male",
        "email" : "mcneilhoover@bugsall.com",
        "phone" : "+1 (847) 416-3400",
        "salary" : 125603,
        "address" : "153 Hausman Street, Homeland, North Carolina, 7806",
        "joinDate" : "2016-04-07T12:40:33 -06:-30",
        "travelledCountries" : [
                "Cameroon",
                "Ethiopia"
        ],
        "greeting" : "Hello, Mcneil Hoover! You have 5 unread messages."
}
{
        "_id" : ObjectId("5f631528819dc2c0f40c643c"),
        "employeeId" : "fd0806f2-f77d-46f1-af1b-2fdf2b3a24de",
        "picture" : "http://placehold.it/32x32",
        "age" : 33,
        "eyeColor" : "brown",
        "name" : "Ola Donovan",
        "gender" : "female",
        "email" : "oladonovan@bugsall.com",
        "phone" : "+1 (845) 505-3759",
        "salary" : 101181,
        "address" : "595 Visitation Place, Bonanza, Marshall Islands, 5365",
        "joinDate" : "2019-10-05T03:32:13 -06:-30",
        "travelledCountries" : [
                "Eritrea",
                "Grenada"
        ],
        "greeting" : "Hello, Ola Donovan! You have 10 unread messages."
}
Type "it" for more



3-Write mongodb query to find all employees with no travel experience.
db.users.find({"travelledCountries":{$size:0}}).count()
283
db.users.find({"travelledCountries":{$size:0}}).pretty()
{
  "_id" : ObjectId("5f631528819dc2c0f40c64a1"),
  "employeeId" : "a674ce1d-51aa-4843-8182-2b4bdaa95435",
  "picture" : "http://placehold.it/32x32",
  "age" : 32,
  "eyeColor" : "blue",
  "name" : "Perry Solomon",
  "gender" : "male",
  "email" : "perrysolomon@bugsall.com",
  "phone" : "+1 (858) 559-3415",
  "salary" : 169108,
  "address" : "914 Belvidere Street, Coyote, Nevada, 7104",
  "joinDate" : "2016-05-06T08:45:09 -06:-30",
  "travelledCountries" : [ ],
  "greeting" : "Hello, Perry Solomon! You have 2 unread messages."
}
{
  "_id" : ObjectId("5f631528819dc2c0f40c64a9"),
  "employeeId" : "411f25d9-2d52-43a0-8feb-b886ac740a58",
  "picture" : "http://placehold.it/32x32",
  "age" : 33,
  "eyeColor" : "blue",
  "name" : "Lina Olsen",
  "gender" : "female",
  "email" : "linaolsen@bugsall.com",
  "phone" : "+1 (815) 452-3105",
  "salary" : 181531,
  "address" : "812 Crescent Street, Oretta, North Dakota, 2034",
  "joinDate" : "2016-11-25T08:10:36 -06:-30",
  "travelledCountries" : [ ],
  "greeting" : "Hello, Lina Olsen! You have 5 unread messages."
}
Type "it" for more

4-Write mongodb query to find all employees who travelled to exactly one country.
db.users.find({"travelledCountries":{$size:1}}).count()
253
db.users.find({"travelledCountries":{$size:1}}).pretty()
{
        "_id" : ObjectId("5f631528819dc2c0f40c643e"),
        "employeeId" : "e2d43408-6a5d-4ad2-be36-2a19effed826",
        "picture" : "http://placehold.it/32x32",
        "age" : 34,
        "eyeColor" : "green",
        "name" : "Grimes Knight",
        "gender" : "male",
        "email" : "grimesknight@bugsall.com",
        "phone" : "+1 (843) 498-3093",
        "salary" : 183911,
        "address" : "582 Stone Avenue, Ribera, Maryland, 555",
        "joinDate" : "2017-03-17T01:32:58 -06:-30",
        "travelledCountries" : [
                "Kiribati"
        ],
        "greeting" : "Hello, Grimes Knight! You have 7 unread messages."
}
{
        "_id" : ObjectId("5f631528819dc2c0f40c6443"),
        "employeeId" : "6e3f55c6-846a-47b7-8c0e-57a8e1cfccb8",
        "picture" : "http://placehold.it/32x32",
        "age" : 31,
        "eyeColor" : "blue",
        "name" : "Frazier Burnett",
        "gender" : "male",
        "email" : "frazierburnett@bugsall.com",
        "phone" : "+1 (977) 548-3342",
        "salary" : 138970,
        "address" : "366 Jefferson Avenue, Magnolia, Minnesota, 3669",
        "joinDate" : "2016-05-03T05:13:45 -06:-30",
        "travelledCountries" : [
                "French Southern Territories"
        ],
        "greeting" : "Hello, Frazier Burnett! You have 9 unread messages."
}
Type "it" for more

5-Find all employees with age between 30 and 35.
db.users.find({"age":{$gt:30, $lt:35}}).count()
248
Add additional field with 'healthInsuranceCovered: 300000'
db.users.updateMany({"age":{$gt:30, $lt:35}},{$set:{"healthInsuranceCovered":300000}})

6.Find all employees with salary greater than 120000.
db.users.find({salary:{$gt:120000}}).count()

7-Find salary per annum aggregated by age.
db.users.aggregate([ 
        {$group:{_id:"$age","total_Cost":{$sum:"$salary"}}},
        {$project:{_id:1,total:{$multiply:["$total_Cost",12]}}}
])

o/p
{ "_id" : 27, "total" : 151946832 }
{ "_id" : 20, "total" : 142530756 }
{ "_id" : 36, "total" : 108621828 }
{ "_id" : 25, "total" : 129712800 }
{ "_id" : 28, "total" : 134353644 }

8-Find salary of employees aggregated by travelledCountries.
db.users.aggregate([
        {$group: {_id:{travelledCountries: "$travelledCountries"},"salary":{$sum: "$salary"}}}
])
o/p
{ "_id" : { "travelledCountries" : [ "Central African Republic" ] }, "salary" : 134069 }
{ "_id" : { "travelledCountries" : [ "Poland", "Saint Lucia", "Mexico", "Northern Mariana Islands" ] }, "salary" : 114186 }
{ "_id" : { "travelledCountries" : [ "Oman", "Poland" ] }, "salary" : 158252 }
{ "_id" : { "travelledCountries" : [ "Sao Tome and Principe", "Bangladesh" ] }, "salary" : 451274 }
{ "_id" : { "travelledCountries" : [ "Argentina", "British Indian Ocean Territory", "Bermuda" ] }, "salary" : 280181 }
{ "_id" : { "travelledCountries" : [ "Uzbekistan", "Falkland Islands (Malvinas)", "Turkmenistan", "Pakistan", "Oman" ] }, "salary" : 140414 }
{ "_id" : { "travelledCountries" : [ "Djibouti", "Guinea", "Germany", "Norfolk Island", "Romania" ] }, "salary" : 142149 }
Type "it" for more  

9-Find all employees who have read all emails


10-Find all employees who have only one email to read.



11-Find all email domains with number of emails pending to read.
Example: (not actual count)
bugsall.com: 4

12-Find salary aggregated by area code (in phone number)
        db.users.aggregate([
                {$group:{_id:{phone:"$phone" ,salary:"$salary"}}},
                // {$count:"total"}
         ])
o/p-
{ "_id" : { "phone" : "+1 (978) 491-2847", "salary" : 109054 } }
{ "_id" : { "phone" : "+1 (934) 474-3030", "salary" : 139977 } }
{ "_id" : { "phone" : "+1 (875) 429-3996", "salary" : 182195 } }
{ "_id" : { "phone" : "+1 (870) 508-3049", "salary" : 154908 } }
{ "_id" : { "phone" : "+1 (953) 557-3283", "salary" : 184188 } }
Type "it" for more

13-Find salary aggregated by Zip/Pin code (in address)
db.users.aggregate([
       {$group:{_id:{address:"$address" ,salary:"$salary"}}},
       // {$count:"total"}
])
o/p-
{ "_id" : { "address" : "661 Seba Avenue, Forestburg, Palau, 8243", "salary" : 138192 } }
{ "_id" : { "address" : "629 Lorraine Street, Eagleville, Alabama, 6066", "salary" : 157715 } }
{ "_id" : { "address" : "226 Coffey Street, Buxton, Florida, 6902", "salary" : 129991 } }
{ "_id" : { "address" : "614 Ferris Street, Bethpage, Vermont, 5591", "salary" : 134548 } }
{ "_id" : { "address" : "191 Whitwell Place, Fruitdale, Idaho, 5083", "salary" : 137299 } }
Type "it" for more
 
14-Find salary aggregated by employee whose joinng date is between 2016 & 2020.
db.users.aggregate([
        {$match:{joinDate:{$gte:"2016", $lte:"2020"}}},
        {$group:{_id:{joinDate:"$joinDate",salary:"$salary"}}},
       // {$count:"total"}
])
o/p-
{ "_id" : { "joinDate" : "2017-12-20T11:05:29 -06:-30", "salary" : 169289 } }
{ "_id" : { "joinDate" : "2017-02-11T05:17:56 -06:-30", "salary" : 191264 } }
{ "_id" : { "joinDate" : "2018-09-15T03:20:28 -06:-30", "salary" : 195325 } }
{ "_id" : { "joinDate" : "2017-06-14T12:51:08 -06:-30", "salary" : 176478 } }
Type "it" for more
